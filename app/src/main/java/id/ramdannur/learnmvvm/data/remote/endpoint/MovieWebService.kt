package id.ramdannur.learnmvvm.data.remote.endpoint

import id.ramdannur.learnmvvm.BuildConfig
import id.ramdannur.learnmvvm.data.remote.dto.MovieListDto
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Ramdannur on 6/9/2019.
 */


interface MovieWebService {

    @GET("movie/now_playing?api_key=${BuildConfig.API_KEY}")
    fun getNowplayingMovies(): Deferred<MovieListDto>

    @GET("movie/upcoming?api_key=${BuildConfig.API_KEY}")
    fun getUpcomingMovies(): Deferred<MovieListDto>

    @GET("search/movie?api_key=${BuildConfig.API_KEY}")
    fun searchMovies(@Query("query") query: String): Deferred<MovieListDto>

}