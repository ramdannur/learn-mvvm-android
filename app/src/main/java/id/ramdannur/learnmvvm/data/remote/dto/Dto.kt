package id.ramdannur.learnmvvm.data.remote.dto

import com.google.gson.annotations.SerializedName

/**
 * Created by Ramdannur on 6/9/2019.
 */

open class Dto(
    var totalResults: Long? = null,

    @SerializedName("Response")
    var response: Boolean? = null,

    @SerializedName("Error")
    var error: String? = null
)