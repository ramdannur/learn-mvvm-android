package id.ramdannur.learnmvvm.data

import id.ramdannur.learnmvvm.data.remote.dto.MovieListDto
import id.ramdannur.learnmvvm.data.remote.endpoint.MovieWebService

/**
 * Created by Ramdannur on 6/9/2019.
 */

class MovieRepository(private val remoteDataSource: MovieWebService) {

    suspend fun nowplayingMovies(): MovieListDto {
        //remote data source Request
        return remoteDataSource.getNowplayingMovies().await()
    }

    suspend fun upcomingMovies(): MovieListDto {
        //remote data source Request
        return remoteDataSource.getUpcomingMovies().await()
    }

    suspend fun searchMovies(query: String): MovieListDto {
        //remote data source Request
        return remoteDataSource.searchMovies(query).await()
    }

    suspend fun queryDatabase() {
        //Database query example
//        val sizeDeffered = async {
//            localDataSource.rateDao().getRates().size
//        }
//        Timber.e("erro: ${sizeDeffered.await()}")
    }
}