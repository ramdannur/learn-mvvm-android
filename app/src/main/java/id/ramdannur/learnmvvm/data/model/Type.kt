package id.ramdannur.learnmvvm.data.model

/**
 * Created by Ramdannur on 6/9/2019.
 */

enum class Type(val value: String) {
    MOVIE("movie"),
    SERIES("series"),
    EPISODE("episode"),
    NOWPLAYING("now_playing"),
    UPCOMING("upcoming"),

}