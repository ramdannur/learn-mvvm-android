package id.ramdannur.learnmvvm.data.remote.dto

import com.google.gson.annotations.SerializedName
import id.ramdannur.learnmvvm.data.model.Movie
import java.util.*

/**
 * Created by Ramdannur on 6/9/2019.
 */

class MovieListDto : Dto() {
    @SerializedName("page")
    var page: Int? = null

    @SerializedName("total_results")
    var totalMovies: Int? = null

    @SerializedName("total_pages")
    var totalPages: Int? = null

    @SerializedName("results")
    var movies: ArrayList<Movie>? = null
}