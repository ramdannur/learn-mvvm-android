package id.ramdannur.learnmvvm

import android.app.Application
import id.ramdannur.learnmvvm.injection.module.remoteDatasourceModule
import id.ramdannur.learnmvvm.injection.module.repositoryModule
import id.ramdannur.learnmvvm.injection.module.viewModelModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber

/**
 * Created by Ramdannur on 6/9/2019.
 */

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin(
            this, listOf(
                // inject module
                remoteDatasourceModule,
                repositoryModule,
                viewModelModule
            )
        )
    }
}