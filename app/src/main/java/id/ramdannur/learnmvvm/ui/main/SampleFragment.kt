package id.ramdannur.learnmvvm.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.ramdannur.learnmvvm.R
import id.ramdannur.learnmvvm.ui.BaseFragment
import org.koin.android.architecture.ext.viewModel

/**
 * Created by Ramdannur on 6/12/2019.
 */

class SampleFragment : BaseFragment() {

    val viewModel: MainViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sample, container, false)
    }


}