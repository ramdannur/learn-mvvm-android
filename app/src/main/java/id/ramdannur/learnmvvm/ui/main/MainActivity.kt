package id.ramdannur.learnmvvm.ui.main

import android.os.Bundle
import id.ramdannur.learnmvvm.R
import id.ramdannur.learnmvvm.ui.BaseActivity
import id.ramdannur.learnmvvm.util.ViewConstants
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.architecture.ext.viewModel

class MainActivity : BaseActivity() {

    val viewModel: MainViewModel by viewModel()

    /**
     * Selected menu id. Used to maintain state during the configuration changing
     */
    var menuItemId = R.id.navigation_nowplaying

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupBottomNavigationMenun()

        viewModel.start()

        viewModel.getMovieList()
    }

    private fun setupBottomNavigationMenun() {
        bottomMenu.setOnNavigationItemSelectedListener {
            when {
                it.itemId == R.id.navigation_nowplaying -> {
                    showNowplayingFragment()
                }
                it.itemId == R.id.navigation_upcoming -> {
                    showSampleFragment()
                }
                it.itemId == R.id.navigation_search -> {
                    showSampleFragment()
                }
            }
            true
        }
        bottomMenu.selectedItemId = menuItemId
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(ViewConstants.BOTTOM_NAVIGATION_MENU_INDEX, bottomMenu.selectedItemId)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        savedInstanceState?.let {
            menuItemId = it.get(ViewConstants.BOTTOM_NAVIGATION_MENU_INDEX) as Int
            bottomMenu.selectedItemId = menuItemId
        }
    }

    private fun showNowplayingFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, NowplayingFragment())
            .commit()
    }

    private fun showSampleFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, SampleFragment())
            .commit()
    }
}
