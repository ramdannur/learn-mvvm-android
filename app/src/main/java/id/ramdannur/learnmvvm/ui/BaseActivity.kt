package id.ramdannur.learnmvvm.ui

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity

/**
 * Created by Ramdannur on 6/9/2019.
 */

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity()