package id.ramdannur.learnmvvm.ui.main

import android.arch.lifecycle.MutableLiveData
import id.ramdannur.learnmvvm.data.MovieRepository
import id.ramdannur.learnmvvm.data.model.Movie
import id.ramdannur.learnmvvm.ui.AbstractViewModel
import id.ramdannur.learnmvvm.util.ext.launchAsync

/**
 * Created by Ramdannur on 6/10/2019.
 */

class MainViewModel(private val repository: MovieRepository) : AbstractViewModel() {

    val movie = MutableLiveData<List<Movie>>()

    fun getMovieList() {
//        if (query.isEmpty().not()) {

        //Fun isn't suspended, so it's necessary to run in mainthread
        launchAsync {
            try {
                //The data is loading
                setLoading()

                //Request with a suspended repository funcion
                val dtoMovies = repository.nowplayingMovies()

                movie.value = dtoMovies.movies
            } catch (t: Throwable) {
                //An error was throw
                setError(t)
                movie.value = emptyList()
            } finally {
                //Isn't loading anymore
                setLoading(false)
            }
        }

//        } else {
//            movie.value = emptyList()
//        }
    }

    fun start() {
        movie.value = emptyList()
    }
}