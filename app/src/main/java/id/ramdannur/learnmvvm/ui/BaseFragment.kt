package id.ramdannur.learnmvvm.ui

import id.ramdannur.learnmvvm.util.ViewLifecycleFragment

/**
 * Created by Ramdannur on 6/9/2019.
 */

open class BaseFragment : ViewLifecycleFragment()