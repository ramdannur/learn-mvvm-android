package id.ramdannur.learnmvvm.injection.module

import id.ramdannur.learnmvvm.data.MovieRepository
import org.koin.dsl.module.applicationContext

/**
 * Created by Ramdannur on 6/9/2019.
 */

val repositoryModule = applicationContext {
    factory { MovieRepository(get()) }
}