package id.ramdannur.learnmvvm.injection.module

import id.ramdannur.learnmvvm.ui.main.MainViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext

/**
 * Created by Ramdannur on 6/9/2019.
 */

val viewModelModule = applicationContext {
    viewModel {
        MainViewModel(get())
    }
}