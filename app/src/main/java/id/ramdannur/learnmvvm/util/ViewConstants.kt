package id.ramdannur.learnmvvm.util

/**
 * Created by Ramdannur on 6/9/2019.
 */

class ViewConstants {
    companion object {
        const val REQUEST_APP_PERMISSIONS = 1001

        const val BOTTOM_NAVIGATION_MENU_INDEX = "bottomNavigationMenuIndex"
    }
}